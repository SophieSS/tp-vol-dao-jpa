package sopra.vol;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;
import sopra.vol.dao.jpa.ClientDaoJpa;
import sopra.vol.dao.jpa.CompagnieDaoJpa;
import sopra.vol.dao.jpa.PassagerDaoJpa;
import sopra.vol.dao.jpa.ReservationDaoJpa;
import sopra.vol.dao.jpa.VilleDaoJpa;
import sopra.vol.dao.jpa.VolDaoJpa;
import sopra.vol.dao.jpa.VoyageDaoJpa;
import sopra.vol.dao.jpa.VoyageVolDaoJpa;
import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.jpa.AeroportDaoJpa;



public class Application {
	private static Application instance = null;
	
	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-vol-jpa");

	private final IVilleDao villeDao = new VilleDaoJpa();
	private final IVolDao volDao = new VolDaoJpa();
	private final IVoyageDao voyageDao = new VoyageDaoJpa();
	private final IVoyageVolDao voyagevolDao = new VoyageVolDaoJpa();
	private final ICompagnieDao compagnieDao = new CompagnieDaoJpa();
	private final IReservationDao reservationDao = new ReservationDaoJpa();
	private final IClientDao clientDao = new ClientDaoJpa();
	private final IPassagerDao passagerDao = new PassagerDaoJpa();
	private final IAeroportDao aeroportDao = new AeroportDaoJpa();
	
	private Application() {

	}

	public static Application getInstance() {
		if (instance == null) {
			instance = new Application();
		}

		return instance;
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}

	public IVilleDao getVilleDao() {
		return villeDao;
	}

	public IVolDao getVolDao() {
		return volDao;
	}

	public IVoyageDao getVoyageDao() {
		return voyageDao;
	}

	public IReservationDao getReservationDao() {
		return reservationDao;
	}

	public IClientDao getClientDao() {
		return clientDao;
	}

	public IPassagerDao getPassagerDao() {
		return passagerDao;
	}

	public IAeroportDao getAeroportDao() {
		return aeroportDao;
	}

	public IVoyageVolDao getVoyagevolDao() {
		return voyagevolDao;
	}

	public ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}
}
